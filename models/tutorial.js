const mongoose = require('mongoose');

let schema = mongoose.Schema(
    {
        title: String,
        description: String,
        published: Boolean,
        userId: mongoose.Types.ObjectId //save reference of user
    },
    { timestamps: true }
);

const Tutorial = mongoose.model("tutorial", schema);

module.exports = Tutorial;
