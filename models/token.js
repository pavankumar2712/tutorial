const mongoose = require('mongoose');

let schema = mongoose.Schema(
    {
        userId: String,
        token: {
            type: String,
            unique: true
        },
        expiredAt: Date
    },
    { timestamps: true }
);

schema.statics = {
    generateToken
}

const Tutorial = mongoose.model("token", schema);

module.exports = Tutorial;

async function generateToken(user){

}
