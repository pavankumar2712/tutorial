const mongoose = require('mongoose');
let errors = require("errors/index");
let ValidationError = errors.ValidationError;
const crypto = require('crypto');
const config = require('config');

let schema = mongoose.Schema(
    {
        firstName: String,
        lastName: String,
        email: {
            type: String,
            unique: true
        },
        hash: {
            type: String,
            required: true
        },
        salt: {
            type: String,
            required: true
        }
    },
    { timestamps: true }
);

schema.statics = {
    createUser,
    validateUser
};

const User = mongoose.model("user", schema);

module.exports = User;

async function createUser(user) {
    let isExists = await User.findOne({email: user.email}).exec();
    if(isExists){
        throw new ValidationError("Email Already Exists");
    }
    isExists = new User(user);
    if(!user.password){
        throw new ValidationError("Password Required!");
    }
    //generate hash and save salt
    return await isExists.save();
}

async function validateUser(user) {
    let isExists = await User.findOne({email: user.email}).exec();
    if(!isExists){
        throw new ValidationError("Email not found!");
    }
    //validate hash if hash matches return user
}