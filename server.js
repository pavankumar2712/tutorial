require('app-module-path').addPath(__dirname);
const express = require("express");
const cors = require("cors");
const config = require('config');
let events = require('events');
let eventEmitter = new events.EventEmitter();
const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

require('./bootstrap/db')(config, eventEmitter);

eventEmitter.once('db-connection-established', function () {
    startApp();
});

function startApp() {
    require('./controllers/index')(app, config);
    app.listen(config.port, () => {
        console.log(`Server is running on port ${config.port}.`);
    });
}
