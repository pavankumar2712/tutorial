/**
 * Created by pavan on 16/8/18.
 */
'use strict';
class AuthenticationFailure extends Error {
    constructor(message) {
        super(message);
    }
}

module.exports = AuthenticationFailure;