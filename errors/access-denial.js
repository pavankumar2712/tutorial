/**
 * Created by thespidy on 12/12/16.
 */
'use strict';
class AccessDenialError extends Error {
    constructor(message) {
        if(!message){
            message = "Access Denied";
        }
        super(message);
    }
    static validateUpdationPermission(orderVisibleTo = [], bidVisibleTo = [], organizationId) {
        let canUpdate = false;
        let allOrganizations = orderVisibleTo.concat(bidVisibleTo);
        for(let i = 0; i < allOrganizations.length; i++){
            if(allOrganizations[i].toString() === organizationId.toString()){
                canUpdate = true;
                break;
            }
        }
        if(!canUpdate){
            throw new Error("Access Denied");
        }
    }
}

module.exports = AccessDenialError;