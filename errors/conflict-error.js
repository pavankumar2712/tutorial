/**
 * Created by thespidy on 19/12/16.
 */
'use strict';
class ConflictError extends Error {
    constructor(message) {
        super(message);
    }
}

module.exports = ConflictError;