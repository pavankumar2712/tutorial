const mongoose = require('mongoose');

module.exports = function (config, eventEmitter) {
    function connect() {
        let options = {
            auto_reconnect: true,
            poolSize: 10
        };
        options.useNewUrlParser = true;
        options.useUnifiedTopology = true;
        mongoose.connect(config.mongoose.uri, options);
    }
    
    connect();
    
    mongoose.connection.on('disconnected', connect);
    
    mongoose.connection.on('error', console.error.bind(console, 'connection error:', config.mongoose.uri));
    
    mongoose.connection.once('open', function dbConnectionOpenCallback() {
        console.log('db connected-->', config.mongoose.uri);
        eventEmitter.emit('db-connection-established');
    });
    
    return mongoose;
};