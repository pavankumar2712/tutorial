let errors = require("errors/index");
let ValidationError = errors.ValidationError;
const UserModel = require('models/user');
const TokenModel = require('models/token');

module.exports = {
    createUser,
    generateToken
};

async function createUser(req, res, next) {
    try{
        if(!req.body.email){
            throw new ValidationError("Email Required");
        }
        if(!req.body.firstName){
            throw new ValidationError("First Name Required");
        }
        if(!req.body.lastName){
            throw new ValidationError("Last Name Required");
        }
        res.data = await UserModel.createUser(req.body);
        next();
    }catch (e) {
        errors.handleException(e, next)
    }
}

async function generateToken(req, res, next) {
    //check phone number if already exists throw error
    //if user and password is correct, generate token and save in token collection
    try{
        if(!req.body.email){
            throw new ValidationError("Email Required");
        }
        if(!req.body.password){
            throw new ValidationError("Password Required");
        }
        let user = await UserModel.validateUser(req.body);
        res.data = await TokenModel.generateToken(user);
        next();
    }catch (e) {
        errors.handleException(e, next)
    }
}
