const OauthController = require('./oauth');

module.exports = [
    {
        path: '/sign-up',
        method: 'post',
        public: true,
        controller: OauthController.createUser
    },
    {
        path: '/login',
        method: 'post',
        public: true,
        controller: OauthController.generateToken
    }
];