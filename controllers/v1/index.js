var _ = require('lodash');
var fs = require('fs');
var path = require('path');
var dirs = getFolders(__dirname);
var routes = [];

_(dirs).forEach(function forEachDir(dir) {
    var dirRoutes = require('./' + dir);
    if (_.isFunction(dirRoutes)) {
        dirRoutes = dirRoutes.call();
    }
    _.map(dirRoutes, function forEachRoute(route) {
        route.path = '/' + dir + route.path;
    });
    routes = _.concat(routes, dirRoutes);
    
});

module.exports = routes;

//TODO: This is duplication of function definition, will deal with this later
function getFolders(srcpath) {
    return fs.readdirSync(srcpath).filter(function (file) {
        return fs.statSync(path.join(srcpath, file)).isDirectory();
    });
}
