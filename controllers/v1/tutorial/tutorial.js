const errors = require('errors');
const Tutorial = require('models/tutorial');

module.exports = {
  get
};

async function get(req, res, next) {
    try{
       res.data = await Tutorial.find({userId: req.user._id}).skip(req.filters.skip).limit(req.filters.limit).exec();
       next();
    }catch (e) {
        errors.handleException(e, next)
    }
}