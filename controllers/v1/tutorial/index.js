const tutorialController = require('./tutorial');

module.exports = [
    {
        path: '/',
        name: 'Get Tutorial List',
        method: 'get',
        public: false,
        controller: tutorialController.get
    }
];