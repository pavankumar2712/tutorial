'use strict';
let _ = require('lodash');
let fs = require('fs');
let path = require('path');
let errors = require("errors/index");
let ValidationError = errors.ValidationError;
function headersSent(req, res, next) {
    if ("data" in res) {
        return next('route');
    }
    return next();
}

function requestParser(req, res, next){
    //inifinite scrolling or pagination
    req.filters = {
        limit: 20,
        skip: Number((req.query.skip || 0)),
        sort: (req.query.sort || {_id: -1})
    };
    next();
}

async function authentication(req, res, next){
    req.scope = this || {};
    if(req.scope.public){
        return next();
    }
    let header = req.headers['Authorization'];
    //get from Token collection
    let token = "123456"; //get from DB
    if(!token){
        res.status = 401;
        next({message: "Authorization Failure"});
    }
    //get user from user collection
    req.user = {}; //set user here
    next();
}

module.exports = async function mainController(app, config) {
    let dirs = getFolders(__dirname);
    let routes = [];
    _(dirs).forEach(function forEachDir(dir) {
        let dirRoutes = require('./' + dir);
        _.map(dirRoutes, function forEachRoute(route) {
            if (route.path.slice(-1) === "/") {
                route.path = route.path.slice(0, -1);
            }
            route.path = '/' + dir + route.path;
        });
        routes = _.concat(routes, dirRoutes);
        routes.push({
            path: "/" + dir,
            name: "API " + dir + " root",
            method: "get",
            public: true,
            controller: function defaultHandler(req, res, next) {
                res.data = "Test API " + dir;
                next();
            }
        });
    });

    function postHandler(req, res, next) {
        if (req.route && req.route.path) {
            res.json({
                success: true,
                resultSize: res.resultSize,
                data: res.data
            });
            return res.end();
        } else {
            return next();
        }
    }

    /* handle 404*/
    function notFoundHandler(req, res, next) {
        if (!req.route || !req.route.path) {
            console.log(req.params);
            res.status(404);
            console.log('%s %d %s', req.method, res.statusCode, req.url);
            return res.json({
                error: 'Not found'
            });
        }
    }

    /*
     * error handlers
     */
    function errorHandler(err, req, res, next) {
        let statusCode = err.status || 422;
        if (err.name === 'MongoError' && _.startsWith(err.message, 'E11000')) {
            res.status(statusCode);
        } else if(err.message === "Eway Bill Not found!"){
            res.status(404);
        } else if (err instanceof ValidationError) {
            res.status(statusCode);
        } else {
            var request = {url: req.url};
            request.error = err;
            request.method = req.method;
            if (req.body) {
                request.body = req.body;
            }
            if (req.params) {
                request.params = req.params;
            }
            if (req.query) {
                request.query = req.query;
            }
            statusCode = req.customStatus || err.status || 500;
            res.status(statusCode);
        }
        console.error("Response code ", statusCode);
        console.error(err);
        return res.json({
            success: false,
            error: {
                message: err.message,
                data: err.data
            }
        });
    }

    let router = require('express').Router({mergeParams: true});
    _(routes).forEach(function (route) {
        router[route.method](route.path, [requestParser, authentication.bind(route), headersSent, route.controller, postHandler]);
        if (!route.hasOwnProperty('public')) {
            route.public = false;
        }
        if (!route.hasOwnProperty('allUsers')) {
            route.allUsers = false;
        }
    });
    app.use("/", router);
    app.use(postHandler);
    app.use(notFoundHandler);
    app.use(errorHandler);
};

function getFolders(srcpath) {
    return fs.readdirSync(srcpath).filter(function (file) {
        return fs.statSync(path.join(srcpath, file)).isDirectory();
    });
}


