async function DbFunction() {
    let a = 0;
    await new Promise(function (resolve, reject) {
        setTimeout(function () {
            a = 10;
            resolve();
        })
    });
    return a;
}
async function timer(){
    let a = await DbFunction();
    console.log(a);
}

timer();